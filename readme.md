## YouTube video transcripts
All of these are video transcripts of videos I have on my YouTube channel
https://www.youtube.com/@uplinux4986/
UP Linux

## YouTube discription
Description
Welcome to UP Linux,
This is your host Noah Hill, join me on my adventures learning and coding in different programming langs. with some GNU/Linux distro reviews sprinkled in between.
I'm also on Odysee under the same handle if that is you're preferred viewing platform

GitLab: https://gitlab.com/noahhill1192  -  GitLab is a free (and/or paid!) way for users to upload and download repositories and their files. On my GitLab, you will find not only config files but also different test scripts in Python, JavaScript, C, C++, BASH, BAT, PS1, and Haskell.


Linux Youtubers you need to check out:
Derek Taylor - https://www.youtube.com/DistroTube
The Linux Experiment - https://www.youtube.com/c/TheLinuxExperiment

GNU/Linux
Free, as in free beer


Software and Hardwear I use
Dell Optiplex 3010
	Ubuntu-server = OS
	OBS-Studio = video+audio
	Audacity = audio
	Virt-manager (Linux only) = Virtual Systems / VMs
